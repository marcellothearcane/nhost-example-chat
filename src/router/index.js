import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: {
      authenticated: false,
    },
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import(/* webpackChunkName: "logout" */ '../views/Logout.vue'),
    meta: {
      authenticated: false,
    },
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '../views/Settings.vue'),
    meta: {
      authenticated: true,
    },
  },
  {
    path: '/',
    name: 'Chats',
    component: () => import(/* webpackChunkName: "chats" */ '../views/Chats.vue'),
    meta: {
      authenticated: true,
    },
  },
  {
    path: '/chat/:id',
    name: 'Chat',
    component: () => import(/* webpackChunkName: "chat" */ '../views/Chat.vue'),
    meta: {
      authenticated: true,
    },
  },
  {
    path: '*',
    name: '404',
    component: () => import(/* webpackChunkName: "404" */ '../views/404.vue'),
    meta: {
      authenticated: false,
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, _from, next) => {
  if (to.meta.authenticated && localStorage.getItem('loggedIn') === 'false') {
    console.log('Should be authenticated, but not')
    next('/login')
  } else {
    next()
  }
})

export default router
