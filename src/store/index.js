require('dotenv').config()
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: {
      state () {
        return {
          loggedIn: !!localStorage.getItem('jwt_token'),
          jwt_token: localStorage.getItem('jwt_token'),
          refresh_token: localStorage.getItem('refresh_token'),
          refresh_timer: null,
        }
      },
      mutations: {
        SET_STATE (state, [key, value]) {
          localStorage.setItem(key, value)
          state[key] = value
        },
      },
      actions: {
        async login ({ commit, dispatch }, { username, password }) {
          try {
            const { data } = await axios.post(`https://backend-8j89cbc4.nhost.io/auth/local/login`, {
              username,
              password,
            })

            commit('SET_STATE', [
              'jwt_token',
              data.jwt_token,
            ])
            commit('SET_STATE', [
              'refresh_token',
              data.refresh_token,
            ])
            commit('SET_STATE', [
              'loggedIn',
              true,
            ])

            dispatch('refreshLoop')

            return true
          } catch (err) {
            return new Error(err)
          }
        },
        async loginWithRefresh ({ dispatch }) {
          await dispatch('refresh')
          await dispatch('refreshLoop')
        },
        async refresh ({ state, commit, dispatch }) {
          try {
            const { data } = await axios.post(`https://backend-8j89cbc4.nhost.io/auth/refresh-token`, {
              refresh_token: state.refresh_token,
            })

            commit('SET_STATE', [
              'jwt_token',
              data.jwt_token,
            ])
            commit('SET_STATE', [
              'refresh_token',
              data.refresh_token,
            ])
            commit('SET_STATE', [
              'loggedIn',
              true,
            ])
          } catch (err) {
            dispatch('logout')
            return new Error(err.message)
          }
        },
        logout ({ state, commit }) {
          clearInterval(parseInt(state.refresh_timer))

          commit('SET_STATE', [
            'refresh_timer',
            null,
          ])
          commit('SET_STATE', [
            'jwt_token',
            null,
          ])
          commit('SET_STATE', [
            'refresh_token',
            null,
          ])
          commit('SET_STATE', [
            'loggedIn',
            false,
          ])

          return true
        },
        refreshLoop ({ state, commit, dispatch }) {
          if (state.jwt_token) {
            const decoded = JSON.parse(atob(state.jwt_token.split('.')[1]))
  
            const interval = (decoded.exp - decoded.iat) * 500

            const timer = setInterval(function () {
              dispatch('refresh')
            }, interval)

            commit('SET_STATE', [
              'refresh_timer',
              timer,
            ])
          } else {
            dispatch('logout')
          }
        },
      },
    },
  },
})
